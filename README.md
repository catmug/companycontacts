# README #

### What is this repository for? ###

* It is a repository for Company Contacts. A template to add employees to JoosepFactory 
* Version 0.2

### How do I get set up? ###

This is a spring boot project.
Pull the repository and install maven if needed.
In the project path run

$ mvn spring-boot:run

If you are already using the default 8080 port for something then use

cat$ mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=8081'

### Contribution guidelines ###

* Writing tests

   It is necessary

* Code review

    Don't be mean.


### Who do I talk to? ###

* Repo owner