package ee.catmug;

import ee.catmug.models.Company;
import ee.catmug.models.Person;
import ee.catmug.repository.CompanyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ee.catmug.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CompanContactsApplication.class)
@WebAppConfiguration
public class CompanContactsApplicationTests {

    @Autowired
    CompanyRepository companyRepository;


	@Test
	public void contextLoads() {
	}

    @Test
    public void testCompanyRepository() {


        Company companyFirst = companyRepository.getCompanyFirst();

        assert (companyFirst != null);

    }

    @Test
    public void testCompanyEmplyeeNamesAllContainJoosep() {


        Company companyFirst = companyRepository.getCompanyFirst();

        for(Person employee : companyFirst.getEmployees())
        {
            assert (employee.getName().toLowerCase().contains("joosep"));

        }



    }
}
