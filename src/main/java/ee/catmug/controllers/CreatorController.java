package ee.catmug.controllers;

import ee.catmug.models.Person;
import ee.catmug.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jack on 04/12/15.
 */
@Controller
public class CreatorController {

    @Autowired
    CompanyService companyService;

    @RequestMapping(value = "/creator", method = RequestMethod.GET)
    String showDataAboutMe(Model model) {
        model.addAttribute("creator", "Mikk Raudvere IA17");
        return "creator";
    }

}
