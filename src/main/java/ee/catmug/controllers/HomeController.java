package ee.catmug.controllers;

import ee.catmug.models.Person;
import ee.catmug.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jack on 03/12/15.
 * main controller. at the moment the landing page of
 */
@Controller
class HomeController {

    @Autowired
    CompanyService companyService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    String index(Model model) {
        model.addAttribute("company", companyService.getFirstCompany());
        model.addAttribute("person", new Person());
        model.addAttribute("addressInput", "");

        return "index";
    }
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String addEmployee(@ModelAttribute Person person,@ModelAttribute String addressInput, Model model) {

        person.getAddress().setLocation(addressInput);
        companyService.addEmployee(companyService.getFirstCompany(), person);
        model.addAttribute("company", companyService.getFirstCompany());


        return "index";
    }
}
