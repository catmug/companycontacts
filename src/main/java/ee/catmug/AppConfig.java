package ee.catmug;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jack on 29/11/15.
 */
@EnableAutoConfiguration
@ComponentScan
@Configuration
public class AppConfig {

}
