package ee.catmug.repository;

import ee.catmug.models.Company;

/**
 * Created by jack on 03/12/15.
 */
public interface CompanyRepository {
    Company getCompanyFirst();
    void saveCompany(Company company);
}
