package ee.catmug.repository;

import ee.catmug.models.Address;
import ee.catmug.models.Company;
import ee.catmug.models.CompanyType;
import ee.catmug.models.Person;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jack on 03/12/15.
 */

@Repository("companyRepository")
public class MockCompanyRepositoryImpl implements CompanyRepository {

    private Company company;


    public  MockCompanyRepositoryImpl(){
        List<Person> employees = new ArrayList<>();
        employees.add(new Person("Joosep Toots", "39010254523", LocalDate.of(2000, Month.AUGUST, 1), new Address("Meeletuste tn 6", 12332)));
        employees.add(new Person("Joosep Roots", "390102233523", LocalDate.of(2013, Month.JULY, 18), new Address("Meeletuste tn 3", 12332)));
        employees.add(new Person("Joosep Toots", "39010254523", LocalDate.of(1997, Month.AUGUST, 10), new Address("Meeletuste tn 2", 12332)));
        employees.add(new Person("Joosep Heidik", "39010254523", LocalDate.of(1990, Month.JANUARY, 13), new Address("Meeletuste tn 64", 12332)));

        this.company = new Company(new Address("Meeletuste tn 64", 12332), CompanyType.FACTORY, "Joosepfactory AS", employees);

    }

    @Override
    public Company getCompanyFirst() {

        return company;
    }

    @Override
    public void saveCompany(Company company) {
        this.company = company;
    }


}
