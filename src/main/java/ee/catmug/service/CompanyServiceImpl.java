package ee.catmug.service;

import ee.catmug.models.Company;
import ee.catmug.models.Person;
import ee.catmug.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jack on 03/12/15.
 */
@Service("companyService")
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;


    @Override
    public Company getFirstCompany() {
        return companyRepository.getCompanyFirst();
    }

    @Override
    public void addEmployee(Company company, Person person) {
        company.addEmployee(person);
        companyRepository.saveCompany(company);
    }

}
