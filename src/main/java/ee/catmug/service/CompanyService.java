package ee.catmug.service;

import ee.catmug.models.Company;
import ee.catmug.models.Person;
import ee.catmug.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jack on 03/12/15.
 */

public interface CompanyService {

    Company getFirstCompany();
    void addEmployee(Company company, Person person);
}
