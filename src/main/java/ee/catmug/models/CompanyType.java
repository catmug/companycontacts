package ee.catmug.models;

/**
 * Created by jack on 03/12/15.
 * Used for setting company type
 */
public enum CompanyType {
    DEFAULT, FACTORY
}
