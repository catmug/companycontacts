package ee.catmug.models;

import java.time.LocalDate;

/**
 * Created by jack on 03/12/15.
 */
public class Person {
    private String name;
    private String identificationCode;
    private LocalDate dateOfBirth;
    private Address address;

    public Person() {
    }

    public Person(String name, String identificationCode, LocalDate dateOfBirth, Address address) {
        this.name = name;
        this.identificationCode = identificationCode;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentificationCode() {
        return identificationCode;
    }

    public void setIdentificationCode(String identificationCode) {
        this.identificationCode = identificationCode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
