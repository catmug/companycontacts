package ee.catmug.models;

/**
 * Created by jack on 03/12/15.
 * Simple Address pojo
 */
public class Address {
    private String location;
    private int postalIndex;

    public Address() {
    }

    public Address(String location, int postalIndex) {
        this.location = location;
        this.postalIndex = postalIndex;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getPostalIndex() {
        return postalIndex;
    }

    public void setPostalIndex(int postalIndex) {
        this.postalIndex = postalIndex;
    }
}
