package ee.catmug.models;

import java.util.List;

/**
 * Created by jack on 03/12/15.
 * A simple company pojo
 */
public class Company {
    private String name;
    private CompanyType type;
    private Address address;
    private List<Person> employees;


    public Company() {
    }

    public Company(Address address, CompanyType type, String name, List<Person> employees) {
        this.address = address;
        this.type = type;
        this.name = name;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyType getType() {
        return type;
    }

    public void setType(CompanyType type) {
        this.type = type;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    public List<Person> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Person> employees) {
        this.employees = employees;
    }

    public void addEmployee(Person employee){
        this.employees.add(employee);
    }

}
